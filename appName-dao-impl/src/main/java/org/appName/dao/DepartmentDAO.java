package org.appName.dao;

import java.util.List;

import com.mastertheboss.domain.Department;

public interface DepartmentDAO {

    void save(Department department);

    void update(Department department);

    void delete(Department department);

    Department findByID(Long id);

	List<Department> findAll();

}
