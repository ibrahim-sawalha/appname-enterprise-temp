package org.appName.dao.impl;

import java.util.List;

import org.appName.dao.util.CustomHibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.mastertheboss.domain.Department;

@Repository("departmentDAOImpl")
public class DepartmentDAOImpl extends CustomHibernateDaoSupport// implements
// DepartmentDAO
{

    public void save(Department department) {
        getHibernateTemplate().save(department);
    }

    public void update(Department department) {
        getHibernateTemplate().update(department);
    }

    public void delete(Department department) {
        getHibernateTemplate().delete(department);
    }

    public Department findByID(Long id) {
        List list = getHibernateTemplate().find("select o from Department o where o.id=?", id);
        return (Department) list.get(0);
    }

    public List<Department> findAll() {
        List list = getHibernateTemplate().find("select o from Department o");
        return list;
    }

}
