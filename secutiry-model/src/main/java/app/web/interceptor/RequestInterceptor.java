package app.web.interceptor;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class RequestInterceptor implements PhaseListener {

    @Override
    public void afterPhase(PhaseEvent event) {
        System.out.println(event.getPhaseId().getName());
    }

    @Override
    public void beforePhase(PhaseEvent event) {
        System.out.println(event.getPhaseId().getName());
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }

}
