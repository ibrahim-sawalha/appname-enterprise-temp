package app.web.mbean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import security.anno.SecuredManagedBean;
import security.main.AbstractManagedBean;
import security.test.UsersProviderMock;

@ManagedBean
@ViewScoped
@SecuredManagedBean(name = "com.user.manager")
public class MbLogin extends AbstractManagedBean implements Serializable {
    private static final long serialVersionUID = -8693646441673720097L;

    @Override
    protected Class<?> getServiceBean() {
        return this.getClass();
    }

    public String loginAction() {
        System.out.println(getLoginUserVO());
        setLoginUserVO(UsersProviderMock.findLoginUser(getLoginUserVO().getUserName(), getLoginUserVO().getPassword()));

        if (getLoginUserVO() != null) {
            getLoginUserVO().setLoggedIn(true);
            Faces.setSessionAttribute("loginUserVO", getLoginUserVO());
            if (getLoginUserVO().hasRole("ADMIN")) {
                return "/admin/index.xhtml?faces-redirect=true";
            } else {
                return "/dba/index.xhtml?faces-redirect=true";
            }
        } else {
            // Messages.addError(null, "ERROR {0}", "Login Failed, try again.");
            Messages.addGlobal(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Login Failed, try again."));
        }
        return null;
    }

    public String logoutAction() {
        getLoginUserVO().setLoggedIn(false);
        Faces.setSessionAttribute("loginUserVO", null);
        return "logout";
    }

    public String getText() {
        return "Managed bean started";
    }
}
