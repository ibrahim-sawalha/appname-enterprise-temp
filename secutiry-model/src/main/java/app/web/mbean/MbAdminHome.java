package app.web.mbean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import security.anno.SecuredManagedBean;
import security.main.AbstractManagedBean;

@ManagedBean
@ViewScoped
@SecuredManagedBean(name = "com.user.manager")
public class MbAdminHome<V> extends AbstractManagedBean implements Serializable {
    private static final long serialVersionUID = -1856114098019493592L;

    @Override
    protected Class<?> getServiceBean() {
        return this.getClass();
    }

}
