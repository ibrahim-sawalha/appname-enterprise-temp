package app.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import app.web.vo.LoginUserVO;

@WebFilter("/admin/*")
public class LoginUserFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException,
    ServletException {
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpServletRequest request = (HttpServletRequest) req;

        HttpSession session = request.getSession(false);
        LoginUserVO user = null;
        if (session != null) {
            user = (LoginUserVO) session.getAttribute("loginUserVO");
        }
        if (user != null && user.isLoggedIn()) {
            // Logged in.
            if (user.getRoles().contains("ADMIN")) { // does the user have permission?
                System.out.println("Logged in... WITH Permission" + user);
                chain.doFilter(request, response);
                return;
            } else { // no, he does not have permission
                HttpServletResponse httpResponse = response;
                System.out.println("Logged in... NO Permission" + user);
                httpResponse.sendRedirect(request.getContextPath() + "/permission-error.xhtml?faces-redirect=true");
                return;
            }
        }
        HttpServletResponse httpResponse = response;
        httpResponse.sendRedirect(request.getContextPath());
        System.out.println("NOT Logged in..." + user);
        return;
    }

    @Override
    public void destroy() {
    }

}
