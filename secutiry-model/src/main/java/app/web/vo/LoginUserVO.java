package app.web.vo;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import security.bean.impl.UserCredentialsImpl;

@ManagedBean
@SessionScoped
public class LoginUserVO extends UserCredentialsImpl implements Serializable {

    private static final long serialVersionUID = 7906820010780688797L;

    private String text;
    private boolean loggedIn = false;

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LoginUserVO [text=");
        builder.append(text);
        builder.append(", loggedIn=");
        builder.append(loggedIn);
        builder.append("]");
        return builder.toString();
    }

}
