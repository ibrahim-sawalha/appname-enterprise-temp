package security.test;

public class TheMainApplication {
    public static void main(String[] args) {
        TestSecutiryBeanManager t = new TestSecutiryBeanManager();
        System.out.println("isViewEnabled " + t.isViewEnabled());
        System.out.println("isEditEnabled " + t.isEditEnabled());
        System.out.println("isDeleteEnabled " + t.isDeleteEnabled());
        System.out.println("isAddEnabled " + t.isAddEnabled());

        System.out.println(t.isAllowed("custom action"));
        System.out.println("\n");

        TestSecutiryBeanDBA t2 = new TestSecutiryBeanDBA();
        System.out.println("isViewEnabled " + t2.isViewEnabled());
        System.out.println("isEditEnabled " + t2.isEditEnabled());
        System.out.println("isDeleteEnabled " + t2.isDeleteEnabled());
        System.out.println("isAddEnabled " + t2.isAddEnabled());
        System.out.println(t2.isAllowed("custom action"));

    }
}
