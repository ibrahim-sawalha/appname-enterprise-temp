package security.test;

import security.bean.ApplicationRole;
import security.bean.SecurityClass;
import security.bean.SecurityOperation;
import security.bean.impl.ApplicationRoleImpl;
import security.bean.impl.SecurityClassImpl;
import security.bean.impl.SecurityOperationImpl;
import security.provider.SecurityRolesProvider;

public class SecurityRolesProviderMock implements SecurityRolesProvider {

    private static ApplicationRole admin = new ApplicationRoleImpl("ADMIN");
    private static ApplicationRole dba = new ApplicationRoleImpl("DBA");

    public static SecurityClassImpl getAdminTest() {
        SecurityClassImpl securityClassVO = new SecurityClassImpl();
        securityClassVO.setName("com.user.manager");

        // define allowed operations
        SecurityOperation operationCreate = new SecurityOperationImpl("ADD");
        SecurityOperation operationUpdate = new SecurityOperationImpl("EXECUTE");
        SecurityOperation operationDelete = new SecurityOperationImpl("EDIT");

        operationCreate.addAllowedRole(admin);
        operationUpdate.addAllowedRole(admin);
        operationDelete.addAllowedRole(admin);

        securityClassVO.addAllowedOperation(operationCreate);
        securityClassVO.addAllowedOperation(operationUpdate);
        securityClassVO.addAllowedOperation(operationDelete);
        return securityClassVO;
    }

    public static SecurityClassImpl getDBATest() {
        SecurityClassImpl securityClassVO = new SecurityClassImpl();
        securityClassVO.setName("com.user.dba");

        // define allowed operations
        SecurityOperation operationUpdate = new SecurityOperationImpl("EDIT");
        SecurityOperation operationView = new SecurityOperationImpl("VIEW");

        // operationCreate.addAllowedRole(dba);
        operationUpdate.addAllowedRole(dba);
        operationView.addAllowedRole(dba);

        // securityClassVO.addAllowedOperation(operationCreate);
        securityClassVO.addAllowedOperation(operationUpdate);
        securityClassVO.addAllowedOperation(operationView);
        return securityClassVO;
    }

    @Override
    public SecurityClass findByName(String name) {
        if (name.equals("com.user.manager")) {
            return getAdminTest();
        }
        if (name.equals("com.user.dba")) {
            return getDBATest();
        }
        return null;
    }

    public static SecurityRolesProvider getInstance() {
        return new SecurityRolesProviderMock();
    }

}
