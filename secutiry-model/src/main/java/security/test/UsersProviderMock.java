package security.test;

import app.web.vo.LoginUserVO;

public class UsersProviderMock {

    public static LoginUserVO findLoginUser(String userName, String password) {
        LoginUserVO user = new LoginUserVO();
        if (userName.equals("admin") && password.equals("123")) {
            user.setUserName(userName);
            user.setPassword(password);
            user.addRole("ADMIN");
            user.addRole("TEST");
            user.addRole("USER");
            return user;
        } else if (userName.equals("dba") && password.equals("123")) {
            user.setUserName(userName);
            user.setPassword(password);
            user.addRole("DBA");
            return user;
        } else {

            return null;
        }
    }

    public static LoginUserVO findUserByUsername(String userName) {
        String password = "123";
        LoginUserVO user = new LoginUserVO();
        if (userName.equals("admin")) {
            user.setUserName(userName);
            user.setPassword(password);
            user.addRole("ADMIN");
            user.addRole("TEST");
            user.addRole("USER");
            return user;
        } else if (userName.equals("dba")) {
            user.setUserName(userName);
            user.setPassword(password);
            user.addRole("DBA");
            return user;
        } else {

            return null;
        }
    }

}
