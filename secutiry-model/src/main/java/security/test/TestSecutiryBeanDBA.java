package security.test;

import security.anno.SecuredManagedBean;
import security.main.AbstractManagedBean;
import app.web.vo.LoginUserVO;

@SecuredManagedBean(name = "com.user.dba")
public class TestSecutiryBeanDBA extends AbstractManagedBean {

    @Override
    public LoginUserVO getLoginUserVO() {
        LoginUserVO b = new LoginUserVO();
        b.setUserName("admin");
        b.setPassword("123");
        b.addRole("DBA");
        return b;
    }

    @Override
    protected Class<?> getServiceBean() {
        return this.getClass();
    }

}
