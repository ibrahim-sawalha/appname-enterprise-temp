package security.test;

import security.anno.SecuredManagedBean;
import security.main.AbstractManagedBean;
import app.web.vo.LoginUserVO;

@SecuredManagedBean(name = "com.user.manager")
public class TestSecutiryBeanManager extends AbstractManagedBean {

    public boolean isMethodAllowedView() {
        return isAllowed("TRANSFER");
    }

    @Override
    public LoginUserVO getLoginUserVO() {
        LoginUserVO b = new LoginUserVO();
        b.setUserName("admin");
        b.setPassword("123");
        b.addRole("ADMIN");
        return b;
    }

    @Override
    protected Class<?> getServiceBean() {
        return this.getClass();
    }

}
