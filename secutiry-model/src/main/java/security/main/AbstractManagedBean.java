package security.main;

import java.lang.annotation.Annotation;

import javax.faces.bean.ManagedProperty;

import security.anno.SecuredManagedBean;
import security.bean.SecurityOperation;
import security.provider.SecurityRolesProvider;
import security.test.SecurityRolesProviderMock;
import app.web.vo.LoginUserVO;

public abstract class AbstractManagedBean {

    @ManagedProperty(value = "#{loginUserVO}")
    private LoginUserVO loginUserVO;

    public LoginUserVO getLoginUserVO() {
        if (loginUserVO == null) {
            loginUserVO = new LoginUserVO();
            loginUserVO.setText("init");
        }
        return loginUserVO;
    }

    public void setLoginUserVO(LoginUserVO loginUserVO) {
        this.loginUserVO = loginUserVO;
    }

    protected abstract Class<?> getServiceBean();

    private SecurityRolesProvider getSecurityProvider() {
        return SecurityRolesProviderMock.getInstance();
    }

    public boolean isAllowed(String operation) {
        if (getServiceBean().isAnnotationPresent(SecuredManagedBean.class)) {
            Annotation annotation = getServiceBean().getAnnotation(SecuredManagedBean.class);
            SecuredManagedBean sec = (SecuredManagedBean) annotation;
            security.bean.SecurityClass classVO = getSecurityProvider().findByName(sec.name());
            if (classVO == null) {
                return true;
            }

            SecurityOperation op = classVO.findAllowedOperation(operation);

            if (op != null) {
                return op.isUserAuthorized(getLoginUserVO());
                // return
                // op.getAllowedRoles().contains(getUserBean().getRoles());
            }

            return false;
        } else {
            return true;
        }
    }

    public boolean isViewEnabled() {
        return isAllowed("VIEW");
    }

    public boolean isAddEnabled() {
        return isAllowed("ADD");
    }

    public boolean isEditEnabled() {
        return isAllowed("EDIT");
    }

    public boolean isDeleteEnabled() {
        return isAllowed("DELETE");
    }

    public boolean isPrintEnabled() {
        return isAllowed("PRINT");
    }

    public boolean isExecuteEnabled() {
        return isAllowed("EXECUTE");
    }
}
