package security.provider;

import security.bean.SecurityClass;

public interface SecurityRolesProvider {

	public SecurityClass findByName(String name);

}
