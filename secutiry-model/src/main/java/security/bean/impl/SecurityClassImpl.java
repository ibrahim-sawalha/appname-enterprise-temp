package security.bean.impl;

import java.util.ArrayList;
import java.util.List;

import security.bean.SecurityClass;
import security.bean.SecurityOperation;

public class SecurityClassImpl implements SecurityClass {

	private String name;
	private List<SecurityOperation> operations;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SecurityOperation> getAllowedOperations() {
		if (operations == null) {
			operations = new ArrayList<SecurityOperation>();
		}
		return operations;
	}

	public void setAllowedOperations(List<SecurityOperation> operations) {
		this.operations = operations;
	}

	public void addAllowedOperation(SecurityOperation operation) {
		getAllowedOperations().add(operation);
	}

	public SecurityOperation findAllowedOperation(String name) {
		for (SecurityOperation operation : getAllowedOperations()) {
			if (operation.getName().equals(name)) {
				return operation;
			}
		}
		return null;
	}

}
