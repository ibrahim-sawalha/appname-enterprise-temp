package security.bean.impl;

import security.bean.ApplicationRole;

public class ApplicationRoleImpl implements ApplicationRole {

	private String roleName;

	public String getRoleName() {
		return roleName;
	}

	public ApplicationRoleImpl(String roleName) {
		super();
		this.roleName = roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}