package security.bean;

import java.util.List;

public interface SecurityClass {

	public String getName();

	public void setName(String name);

	public List<SecurityOperation> getAllowedOperations();

	public void setAllowedOperations(List<SecurityOperation> operations);

	public void addAllowedOperation(SecurityOperation operation);

	public SecurityOperation findAllowedOperation(String name);

}
