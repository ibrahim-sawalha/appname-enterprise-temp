package security.bean;


public interface ApplicationRole {

	public String getRoleName();

	public void setRoleName(String roleName);

}
