package security.server;

import java.util.ArrayList;
import java.util.List;

public class ApplicationSecurityProvider {

	public static List<String> getAuthorizedRoles(String beanName) {
		List<String> roles = new ArrayList<String>();
		roles.add("ADMIN");
		roles.add("DBA");
		return roles;
	}

}
