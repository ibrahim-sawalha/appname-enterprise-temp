package com.jsf.spring.scope;

import java.util.Map;

import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan("org.appName")
public class JsfSrpingCustomScopeConfigurer extends CustomScopeConfigurer {

	@Override
	public void setScopes(Map scopes) {
		super.setScopes(scopes);
		scopes.put("view", new com.jsf.spring.scope.ViewScope());
	}

}
