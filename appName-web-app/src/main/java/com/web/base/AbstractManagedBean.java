package com.web.base;

import javax.faces.bean.ManagedProperty;

import com.security.ann.SecurityRolesProvider;
import com.security.bean.UserCredentials;
import com.security.bean.impl.AbstractSecurityManager;
import com.web.vo.VoUser;

public abstract class AbstractManagedBean extends AbstractSecurityManager {

    @ManagedProperty(value = "#{voUser}")
    private VoUser voUser;

    @Override
    protected UserCredentials getUserCredentials() {
        return getVoUser();
    }

    public VoUser getVoUser() {
        if (voUser == null) {
            voUser = new VoUser();
        }
        return voUser;
    }

    public void setVoUser(VoUser loginUserVO) {
        this.voUser = loginUserVO;
    }

    @Override
    protected SecurityRolesProvider getSecurityProvider() {
        // return SecurityRolesProviderMock.getInstance();
        return null;
    }

    public boolean isViewEnabled() {
        return isAllowed("VIEW");
    }

    public boolean isAddEnabled() {
        return isAllowed("ADD");
    }

    public boolean isEditEnabled() {
        return isAllowed("EDIT");
    }

    public boolean isDeleteEnabled() {
        return isAllowed("DELETE");
    }

    public boolean isPrintEnabled() {
        return isAllowed("PRINT");
    }

    public boolean isExecuteEnabled() {
        return isAllowed("EXECUTE");
    }
}
