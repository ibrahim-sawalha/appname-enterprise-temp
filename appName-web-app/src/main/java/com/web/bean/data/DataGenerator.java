package com.web.bean.data;

import java.util.ArrayList;
import java.util.List;

import com.web.bean.City;
import com.web.bean.Country;

public class DataGenerator {

    private static List<Country> countries;

    public static List<Country> generateCountries() {

        if (countries == null) {
            countries = new ArrayList<Country>();
            Country jordan = new Country("1", "Jordan");
            jordan.addCity(new City("1", "Amman"));
            jordan.addCity(new City("2", "Irbid"));

            Country saudi = new Country("2", "Saudi Arabia");
            saudi.addCity(new City("3", "Riyadh"));
            saudi.addCity(new City("4", "Jeddah"));

            Country uae = new Country("3", "UAE");
            uae.addCity(new City("4", "Dubia"));
            uae.addCity(new City("5", "Abu Thabi"));

            countries.add(jordan);
            countries.add(saudi);
            countries.add(uae);
        }
        return countries;

    }

}
