package com.web.bean;

import java.io.Serializable;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.CloseEvent;
import org.primefaces.event.MoveEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import com.web.util.FacesUtils;

@ManagedBean(name = "mbLov")
@ViewScoped
public class MbLovExample implements Serializable {
    private static final long serialVersionUID = 55231461850087607L;

    private String lovInput = "";
    private Country selectedCountry;
    private City selectedCity;

    public String getLovInput() {
        return lovInput;
    }

    public void setLovInput(String lovInput) {
        this.lovInput = lovInput;
    }

    public Country getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(Country selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public City getSelectedCity() {
        return selectedCity;
    }

    public void setSelectedCity(City selectedCity) {
        System.out.println("set selected city" + selectedCity);
        this.selectedCity = selectedCity;
    }

    public void onRowSelect(SelectEvent event) {
        setSelectedCity(null);

        // Begin
        // Access ViewScoped Managed Bean
        // Technique-1
        FacesContext context = FacesContext.getCurrentInstance();
        Application app = context.getApplication();
        System.out.println(app.evaluateExpressionGet(context, "#{mbCountry}", MbCountry.class).getSelectedCountry());
        // setSelectedCountry(app.evaluateExpressionGet(context, "#{mbCountry}",
        // MbCountry.class).getSelectedCountry());

        // Technique-2
        System.out.println(((MbCountry) FacesUtils.getManagedBean("mbCountry")).getSelectedCountry());
        setSelectedCountry(((MbCountry) FacesUtils.getManagedBean("mbCountry")).getSelectedCountry());
        // End

        FacesMessage msg = new FacesMessage("Country Selected", ((Country) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowUnselect(UnselectEvent event) {
        System.out.println(event.getObject());
        FacesMessage msg = new FacesMessage("Country Unselected", ((Country) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCityRowSelect(SelectEvent event) {
        System.out.println(event.getObject());
        System.out.println(((MbCountry) FacesUtils.getManagedBean("mbCountry")).getSelectedCountry());
        System.out.println(((MbCountry) FacesUtils.getManagedBean("mbCountry")).getSelectedCity());
        setSelectedCity(((MbCountry) FacesUtils.getManagedBean("mbCountry")).getSelectedCity());
    }

    public void onCityRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("City Unselected", ((City) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void handleClose(CloseEvent event) {
        System.out.println("Dialog was closed..." + event.getComponent().getId());
    }

    public void handleMove(MoveEvent event) {
        System.out.println("Dialog was moved... " + event.getComponent().getId() + " moved Left: " + event.getLeft()
            + ", Top: " + event.getTop());
    }

}
