package com.web.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.web.bean.data.DataGenerator;

@ManagedBean(name = "mbCountry")
@ViewScoped
public class MbCountry implements Serializable {

    private Country selectedCountry;
    private City selectedCity;

    public List<Country> getCountries() {
        return DataGenerator.generateCountries();
    }

    public List<City> getCitites(Country country) {
        System.out.println("get String : " + country);
        if (country != null) {
            return country.getCities();
        }
        return null;
    }

    public Country getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(Country selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public City getSelectedCity() {
        return selectedCity;
    }

    public void setSelectedCity(City selectedCity) {
        this.selectedCity = selectedCity;
    }

}
