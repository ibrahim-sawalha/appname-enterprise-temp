package com.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Country implements Serializable {

    private String id;
    private String name = "";

    private List<City> cities;

    public Country(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCities() {
        if (cities == null) {
            cities = new ArrayList<City>();
        }
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public void addCity(City city) {
        getCities().add(city);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Country [id=");
        builder.append(id);
        builder.append(", name=");
        builder.append(name);
        builder.append(", cities=");
        builder.append(cities);
        builder.append("]");
        return builder.toString();
    }

}
