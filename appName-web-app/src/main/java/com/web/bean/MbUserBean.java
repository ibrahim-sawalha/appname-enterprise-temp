package com.web.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.appName.dao.impl.DepartmentDAOImpl;
import org.appName.service.Bean;
import org.appName.service.DepartmentService;
import org.appName.service.config.PersistenceJPAConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mastertheboss.domain.Department;

@ManagedBean(name = "mbUser")
@SessionScoped
public class MbUserBean implements Serializable {
    private static final long serialVersionUID = 55231461850087607L;

    private List<Department> departmentList;
    @ManagedProperty(value = "#{departmentService}")
    DepartmentService service;

    @ManagedProperty(value = "#{departmentDAOImpl}")
    DepartmentDAOImpl departmentDAO;

    public DepartmentDAOImpl getDepartmentDAO() {
        return departmentDAO;
    }

    public void setDepartmentDAO(DepartmentDAOImpl departmentDAO) {
        this.departmentDAO = departmentDAO;
    }

    public DepartmentService getService() {
        return service;
    }

    public void setService(DepartmentService service) {
        this.service = service;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    public List<Department> getDepartmentList() {
        // test();
        // departmentList = service.findAll();
        departmentList = departmentDAO.findAll();
        System.out.println(departmentList.size());

        return departmentList;
    }

    private void test() {
        ApplicationContext context = new AnnotationConfigApplicationContext(PersistenceJPAConfig.class);

        Bean obj = (Bean) context.getBean("myBean");
        System.out.println(obj.isABean());
        obj.getRecords();

        DepartmentService service = (DepartmentService) context.getBean("departmentService");

        System.out.println(service.findByID(new Long(1)));

        // DepartmentDAO departmentDAO = (DepartmentDAO) context
        // .getBean("departmentDAO");
        // System.out.println(departmentDAO.findByID(new Long(1)));

    }
}
