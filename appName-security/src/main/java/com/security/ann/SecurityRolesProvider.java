package com.security.ann;

import com.security.bean.SecurityClass;

public interface SecurityRolesProvider {

    public SecurityClass findByName(String name);

}
