package com.security.bean;

import java.util.List;

public interface UserCredentials {

	public String getUserName();

	public void setUserName(String userName);

	public String getPassword();

	public void setPassword(String password);

	public List<String> getRoles();

	public void setRoles(List<String> roles);

	public void addRole(String role);

	public boolean hasRole(String role);

	public boolean isEnabled();

	public void setEnabled(boolean flag);

}
