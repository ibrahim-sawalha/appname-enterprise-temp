package com.security.bean;

import java.util.List;

public interface SecurityOperation {

	public String getName();

	public void setName(String name);

	public List<ApplicationRole> getAllowedRoles();

	public void setAllowedRoles(List<ApplicationRole> enabledRoles);

	public void addAllowedRole(ApplicationRole role);

	public boolean isUserAuthorized(UserCredentials user);

}
