package com.security.bean.impl;

import java.util.ArrayList;
import java.util.List;

import com.security.bean.UserCredentials;

public class UserCredentialsImpl implements UserCredentials {

    private String userName;
    private String password;
    private List<String> roles;
    private boolean enabled = true;

    @Override
    public String getUserName() {
        if (userName == null) {
            userName = "";
        }
        return userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String getPassword() {
        if (password == null) {
            password = "";
        }
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public List<String> getRoles() {
        if (roles == null) {
            roles = new ArrayList<String>();
        }
        return roles;
    }

    @Override
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    @Override
    public void addRole(String role) {
        getRoles().add(role);
    }

    @Override
    public boolean hasRole(String role) {
        return getRoles().contains(role);
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean flag) {
        enabled = flag;
    }

    @Override
    public String toString() {
        return "UserCredentialsImpl [userName=" + userName + ", password=" + password + ", roles=" + roles
                + ", enabled=" + enabled + "]";
    }

}
