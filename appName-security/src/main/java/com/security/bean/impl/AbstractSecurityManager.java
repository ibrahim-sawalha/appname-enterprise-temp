package com.security.bean.impl;

import java.lang.annotation.Annotation;

import com.security.ann.SecuredManagedBean;
import com.security.ann.SecurityRolesProvider;
import com.security.bean.SecurityClass;
import com.security.bean.SecurityOperation;
import com.security.bean.UserCredentials;

public abstract class AbstractSecurityManager {

    protected abstract UserCredentials getUserCredentials();

    protected abstract Class<?> getServiceBean();

    protected abstract SecurityRolesProvider getSecurityProvider();

    public boolean isAllowed(String operation) {
        if (getServiceBean().isAnnotationPresent(SecuredManagedBean.class)) {
            Annotation annotation = getServiceBean().getAnnotation(SecuredManagedBean.class);
            SecuredManagedBean sec = (SecuredManagedBean) annotation;
            SecurityClass classVO = getSecurityProvider().findByName(sec.name());
            if (classVO == null) {
                return true;
            }

            SecurityOperation op = classVO.findAllowedOperation(operation);

            if (op != null) {
                return op.isUserAuthorized(getUserCredentials());
                // return
                // op.getAllowedRoles().contains(getUserBean().getRoles());
            }

            return false;
        } else {
            return true;
        }
    }

}
