package com.security.bean.impl;

import java.util.ArrayList;
import java.util.List;

import com.security.bean.ApplicationRole;
import com.security.bean.SecurityOperation;
import com.security.bean.UserCredentials;

public class SecurityOperationImpl implements SecurityOperation {

    private String name;
    private List<ApplicationRole> allowedRoles;

    public SecurityOperationImpl(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ApplicationRole> getAllowedRoles() {
        if (allowedRoles == null) {
            allowedRoles = new ArrayList<ApplicationRole>();
        }
        return allowedRoles;
    }

    public void setAllowedRoles(List<ApplicationRole> enabledRoles) {
        this.allowedRoles = enabledRoles;
    }

    public void addAllowedRole(ApplicationRole role) {
        getAllowedRoles().add(role);
    }

    public boolean isUserAuthorized(UserCredentials user) {
        for (String userRole : user.getRoles()) {
            for (ApplicationRole role : getAllowedRoles()) {
                if (userRole.equals(role.getRoleName())) {
                    return true;
                }
            }
        }
        return false;
    }

}
