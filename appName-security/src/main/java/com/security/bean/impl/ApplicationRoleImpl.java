package com.security.bean.impl;

import com.security.bean.ApplicationRole;

public class ApplicationRoleImpl implements ApplicationRole {

    private String roleName;

    @Override
    public String getRoleName() {
        return roleName;
    }

    public ApplicationRoleImpl(String roleName) {
        super();
        this.roleName = roleName;
    }

    @Override
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
