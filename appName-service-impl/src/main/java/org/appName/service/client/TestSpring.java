package org.appName.service.client;

import org.appName.dao.DepartmentDAO;
import org.appName.service.Bean;
import org.appName.service.DepartmentService;
import org.appName.service.config.PersistenceJPAConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestSpring {

	public void test() {
		ApplicationContext context = new AnnotationConfigApplicationContext(
				PersistenceJPAConfig.class);

		Bean obj = (Bean) context.getBean("myBean");
		System.out.println(obj.isABean());
		obj.getRecords();

		DepartmentService service = (DepartmentService) context
				.getBean("departmentService");

		System.out.println(service.findByID(new Long(1)));

		DepartmentDAO departmentDAO = (DepartmentDAO) context
				.getBean("departmentDAO");
		System.out.println(departmentDAO.findByID(new Long(1)));

	}

	public static void main(String[] args) {
		// ApplicationContext context = new
		// ClassPathXmlApplicationContext("spring/beans.xml");
		new TestSpring().test();
	}
}
