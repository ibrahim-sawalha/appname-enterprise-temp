package org.appName.service;

import java.util.List;

import com.mastertheboss.domain.Department;

public interface DepartmentService {

	void save(Department department);

	void update(Department department);

	void delete(Department department);

	Department findByID(Long id);

	List<Department> findAll();

	String getTestName();

}
