package org.appName.service;

public interface Bean {

    boolean isABean();

    String getRecords();

}
