package org.appName.service.impl;

import java.util.List;

import org.appName.dao.impl.DepartmentDAOImpl;
import org.appName.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mastertheboss.domain.Department;

@Service("departmentService")
@Scope("session")
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentDAOImpl departmentDAO;

    @Override
    public void save(Department department) {
        departmentDAO.save(department);
    }

    @Override
    public void update(Department department) {
        departmentDAO.update(department);
    }

    @Override
    public void delete(Department department) {
        departmentDAO.delete(department);
    }

    @Override
    public Department findByID(Long id) {
        return departmentDAO.findByID(id);
    }

    @Override
    public List<Department> findAll() {
        return departmentDAO.findAll();
    }

    @Override
    public String getTestName() {
        return "TESTING...";
    }

    public Integer getRowsCount() {
        return findAll().size();
    }
}
