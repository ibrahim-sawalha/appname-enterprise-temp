package org.appName.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.appName.service.Bean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mastertheboss.domain.Department;
import com.mastertheboss.domain.Employee;

@Repository
public class BeanImpl implements Bean {

    @PersistenceContext
    protected EntityManager manager;

    @Override
    public boolean isABean() {
        List<Employee> resultList = manager.createQuery("Select a From Employee a", Employee.class).getResultList();
        System.out.println("num of employess:" + resultList.size());
        for (Employee next : resultList) {
            System.out.println("next employee: " + next);
        }
        return true;
    }

    @Override
    @Transactional
    public String getRecords() {
        listEmployees();
        try {
            createEmployees();
        } catch (Exception e) {
            e.printStackTrace();
        }

        listEmployees();
        manager.close();

        System.out.println(".. done");
        return null;
    }

    @Transactional
    private void createEmployees() {
        int numOfEmployees = manager.createQuery("Select a From Employee a", Employee.class).getResultList().size();
        if (numOfEmployees == 0) {
            Department department = new Department("java");
            manager.persist(department);

            manager.persist(new Employee("aloha Gipsz", department));
            manager.persist(new Employee("Captain Nemo", department));
            manager.persist(new Employee("Ibrahim Sawalha", department));

            department = new Department("DBA");
            manager.persist(department);
            manager.persist(new Employee("Ibrahim Sawalha", department));
        }
    }

    @Transactional
    private void listEmployees() {
        List<Employee> resultList = manager.createQuery("Select a From Employee a", Employee.class).getResultList();
        System.out.println("num of employess:" + resultList.size());
        for (Employee next : resultList) {
            System.out.println("next employee: " + next);
        }
    }

}
