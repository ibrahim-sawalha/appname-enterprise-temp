package org.appName.service.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.appName.service.impl.BeanImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan("org.appName")
public class PersistenceJPAConfig {

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setDataSource(this.restDataSource());
		factoryBean
				.setPackagesToScan(new String[] { "com.mastertheboss.domain" });

		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter() {
			{
				setShowSql(true);
				setGenerateDdl(true);
				setDatabase(Database.DERBY);
			}
		};
		factoryBean.setJpaVendorAdapter(vendorAdapter);
		factoryBean.setJpaProperties(getHibernateProperties());

		return factoryBean;
	}

	private Properties getHibernateProperties() {
		Properties pro = new Properties();
		pro.put("hibernate.archive.autodetection", "class");
		pro.put("hibernate.connection.driver_class",
				"org.apache.derby.jdbc.EmbeddedDriver");
		pro.put("hibernate.connection.url",
				"jdbc:derby:target/simpleDb;create=true");
		pro.put("hibernate.show_sql", "true");
		pro.put("hibernate.flushMode", "FLUSH_AUTO");
		pro.put("hibernate.hbm2ddl.auto", "update");
		pro.put("hibernate.dialect", "org.hibernate.dialect.DerbyDialect");
		return pro;
	}

	@Bean
	public DataSource restDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.apache.derby.jdbc.EmbeddedDriver");
		dataSource.setUrl("jdbc:derby:target/simpleDb;create=true");
		dataSource.setUsername("EclipseJPAExample");
		dataSource.setPassword("EclipseJPAExample");
		return dataSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(this
				.entityManagerFactoryBean().getObject());

		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean(name = "myBean")
	public BeanImpl getBean() {
		return new BeanImpl();
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		System.out.println("----------InsessionFactory------------");
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(restDataSource());
		String[] pckage = { "com.mastertheboss.domain" };
		sessionFactory.setPackagesToScan(pckage);
		sessionFactory.setHibernateProperties(getHibernateProperties());
		System.out.println("----------Outof session------------");
		return sessionFactory;
	}

	// @Bean(name = "departmentService")
	// public DepartmentService getDepartmentService() {
	// DepartmentDd = new DepartmentDAOImpl();
	// d.setSessionFactory(sessionFactory().getObject());
	// return d;
	// }
}
